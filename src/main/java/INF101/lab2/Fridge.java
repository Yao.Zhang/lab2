package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    //Instance field (variables)

    int max_size = 20;
    List<FridgeItem> currentItems = new ArrayList<FridgeItem>();

    @Override
    public int totalSize(){
        // Maks antall gjenstander i Fridge.
        return max_size;
    }

    @Override
    public int nItemsInFridge() {
        // Antall gjenstander i Fridge.
        return currentItems.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        // Vil returnere til false hvis det ikke noe mer plass i Fridge,
        // ellers vil item bli plassert i kjøleskapet. 
        if(nItemsInFridge() >= totalSize()){
            return false;
        }
        currentItems.add(item);
        return true;
    }

    @Override
    public void takeOut(FridgeItem item) {
        // Sjekker om Fridge har gjenstander i kjøleskapet. 
        // Hvis det er 0 ting i Fridge så vil det oppstå feilmelding.
        if(nItemsInFridge() < 1){
            throw new NoSuchElementException();
        }
        currentItems.remove(item);
        
    }

    @Override
    public void emptyFridge() {
        //Fjerner alle gjenstader/mat fra Fridge
        currentItems.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        
        List<FridgeItem> removeExpiredFoodInFridge = new ArrayList<FridgeItem>();
        
        for(FridgeItem item : currentItems) {
            if(item.hasExpired())
                removeExpiredFoodInFridge.add(item);
        }
        for(FridgeItem item: removeExpiredFoodInFridge) {
            currentItems.remove(item);
        }
        return removeExpiredFoodInFridge;
    }
}
